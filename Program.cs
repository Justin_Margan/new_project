﻿using System;

namespace demo_01
{
    class Program
    {
        static void Main(string[] args)
        {         
         //Start the program with Clear();
         Console.Clear();
        
         var myName = "Justin";
            Console.WriteLine(myName);
         
         //End the program with blank line and instructions
         Console.ResetColor();
         Console.WriteLine();
         Console.WriteLine("Press <Enter> to quit the program");
         Console.ReadKey();
        
        }
    }
}